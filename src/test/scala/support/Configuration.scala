package support

import java.io.File

import com.typesafe.config.{Config, ConfigFactory}

object Configuration {
  val path = "src/test/resources/gatling.conf"
  val config: Config = ConfigFactory.parseFile(new File(path)).getConfig("gatling")

  val baseUrl = s"${config.getString("host")}/game.web/service"
  val numberOfConcurrentUsers: Int = config.getInt("number_of_concurrent_users")
  val organization: String = config.getString("organization")

  val commonHeaders = Map(
    "Accept" -> "*/*",
    "User-Agent" -> "Gatling"
  )
}
