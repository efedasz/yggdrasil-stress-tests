package simulations.vikings_go_wild

import io.gatling.core.Predef._
import io.gatling.core.structure.ScenarioBuilder
import io.gatling.http.Predef.{http, jsonPath, status}
import io.gatling.http.protocol.HttpProtocolBuilder
import simulations.vikings_go_wild.GameConfiguration._
import support.Configuration

object GameConfiguration {
  val fnAuthenticate = "authenticate"
  val fnPlay = "play"
  val gameId = 7316

  val currency = "EUR"
  val coin = 0.05
  val amount = 1.25
}

class SpinUntilMoneyWonSimulation extends Simulation {
  val httpProtocol: HttpProtocolBuilder = http
    .baseURL(Configuration.baseUrl)

  val scenarioBuilder: ScenarioBuilder = scenario("Spin Until Money Won")
    .exec(
      http("Authenticate")
        .get("")
        .headers(Configuration.commonHeaders)
        .queryParam("fn", fnAuthenticate)
        .queryParam("org", Configuration.organization)
        .queryParam("gameid", gameId)
        .queryParam("currency", currency)
        .check(status.is(200))
        .check(
          jsonPath("$.data.sessid").exists.saveAs("sessionId")
        )
    )
    .pause(1)
    .exec(session => session.set("wonAmount", "0.00"))
    .asLongAs(session => session("wonAmount").as[String] == "0.00") {
      exec(
        http("Play step1")
          .get("")
          .headers(Configuration.commonHeaders)
          .queryParam("fn", fnPlay)
          .queryParam("org", Configuration.organization)
          .queryParam("gameid", gameId)
          .queryParam("currency", currency)
          .queryParam("sessid", "${sessionId}")
          .queryParam("coin", coin)
          .queryParam("amount", amount)
          .check(status.is(200))
          .check(
            jsonPath("$.data.wager.wagerid").exists.saveAs("wagerId")
          )
          .check(
            jsonPath("$.data.wager.status").exists.saveAs("wagerStatus")
          )
      )
      .doIf(session => session("wagerStatus").as[String] == "Pending") {
        exec(
          http("Play step2")
            .get("")
            .headers(Configuration.commonHeaders)
            .queryParam("fn", fnPlay)
            .queryParam("org", Configuration.organization)
            .queryParam("gameid", gameId)
            .queryParam("currency", currency)
            .queryParam("sessid", "${sessionId}")
            .queryParam("wagerid", "${wagerId}")
            .queryParam("betid", 1)
            .queryParam("step", 2)
            .queryParam("cmd", "C")
            .check(status.is(200))
            .check(
              jsonPath("$.data.wager.bets[0].wonamount").exists.saveAs("wonAmount")
            )
        )
      }
    }

  setUp(
    scenarioBuilder.inject(atOnceUsers(Configuration.numberOfConcurrentUsers))
  ).protocols(httpProtocol)
}
